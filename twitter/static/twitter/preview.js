$('.profile_link').popover();

function remove_all_children(tag) {
    while (tag.firstChild) {
        tag.removeChild(tag.firstChild);
    }
}

function restore_original_state(twitt_body_tag, twitt_content) {
    let dismiss_button = twitt_body_tag.querySelector('.dismiss-button');
    let save_button = twitt_body_tag.querySelector('.modify-twitt-button');
    let twitt_content_tag = twitt_body_tag.querySelector('.twitt-content');

    remove_all_children(twitt_content_tag);
    twitt_content_tag.innerText = twitt_content;

    save_button.removeEventListener('click', save_twitt_modification);
    save_button.addEventListener('click', enable_twitt_modification);
    save_button.innerText = "Modify content";

    twitt_body_tag.removeChild(dismiss_button);
}

function save_twitt_modification() {
    let modify_twitt_button = this;
    let content_input_tag = this.parentElement.querySelector('.editable-twitt-content');

    let twitt_pk = this.parentElement.id.toString();
    twitt_pk = twitt_pk.split('#')[1];

    $.ajax({
        type: 'POST',
        url: "http://" + window.location.host + '/ajax/twitt/modify/' + twitt_pk,
        dataType: "json",
        data: {
            new_content: content_input_tag.value,
            csrfmiddlewaretoken: $("[name=csrfmiddlewaretoken]").val(),
        },

        success: function (data) {
            restore_original_state(modify_twitt_button.parentElement,
                content_input_tag.value);
        },

        error: function (data) {
            alert("An error has occurred. Please try again later.");
        },
    });
}

function enable_twitt_modification() {
    let twitt_content_tag = this.parentElement.querySelector('.twitt-content');
    let content = twitt_content_tag.innerText;

    let textarea = document.createElement("textarea");
    textarea.setAttribute("class", "editable-twitt-content");
    textarea.innerText = content;

    remove_all_children(twitt_content_tag);
    twitt_content_tag.appendChild(textarea);
    this.innerText = "Save";

    this.removeEventListener('click', enable_twitt_modification);
    this.addEventListener('click', save_twitt_modification);

    let dismiss_button = document.createElement("button");
    dismiss_button.setAttribute("class", "btn btn-danger ml-1 dismiss-button");
    dismiss_button.innerText = "Dismiss change";
    dismiss_button.addEventListener('click', function() {
        restore_original_state(dismiss_button.parentElement, content);
    });

    this.parentElement.insertBefore(dismiss_button, this.nextSibling);
}

let modify_buttons = document.getElementsByClassName('modify-twitt-button');

Array.prototype.forEach.call(modify_buttons, function(button) {
    button.addEventListener('click', enable_twitt_modification);
});
