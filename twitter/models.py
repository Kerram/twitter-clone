from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone


# Create your models here.
class Twitt(models.Model):
    title = models.CharField(max_length=150)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update_date = models.DateTimeField(default=None, blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.pk:
            self.last_update_date = timezone.now()

        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("twitt-show", kwargs={"pk": self.pk})
