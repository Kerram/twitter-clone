from django.urls import path
from . import views

urlpatterns = [
    path("", views.HomeView.as_view(), name="twitter-home"),
    path("twitt/create", views.CreateTwittView.as_view(), name="twitt-create"),
    path("twitt/show/<int:pk>/", views.ShowTwittView.as_view(), name="twitt-show"),
    path("twitt/update/<int:pk>/", views.UpdateTwittView.as_view(), name="twitt-update"),
    path("twitt/delete/<int:pk>/", views.DeleteTwittView.as_view(), name="twitt-delete"),
    path("twitts/<str:username>/", views.UserTwittsView.as_view(), name="user-twitts-list"),
    path("ajax/twitt/modify/<int:pk>", views.ajax_modify_twitt, name="ajax-modify-twitt"),
]
