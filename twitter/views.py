from django.views.generic import CreateView, DetailView, UpdateView, DeleteView, ListView
from .models import Twitt
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, Http404


# Create your views here.
class HomeView(ListView):
    model = Twitt
    context_object_name = "twitts"
    template_name = "twitter/home.html"
    ordering = ["-creation_date"]
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Home"
        return context


class UserTwittsView(ListView):
    model = Twitt
    template_name = "twitter/users_twitts.html"
    context_object_name = "twitts"
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get("username"))
        return Twitt.objects.filter(author=user).order_by("-creation_date")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = self.kwargs["username"] + " twitts"
        return context


class CreateTwittView(LoginRequiredMixin, CreateView):
    model = Twitt
    fields = ["title", "content"]
    template_name = "twitter/twitt_create.html"

    def form_valid(self, form):
        form.instance.author = self.request.user
        messages.success(self.request, "You successfully created a new Twitt!")
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Create twitt"
        return context


class ShowTwittView(DetailView):
    model = Twitt

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Twitt details"
        return context


class UpdateTwittView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Twitt
    fields = ["title", "content"]
    template_name = "twitter/twitt_update.html"

    def form_valid(self, form):
        form.instance.author = self.request.user
        messages.success(self.request, "Twitt update successful!")
        return super().form_valid(form)

    def test_func(self):
        return self.get_object().author == self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Update twitt"
        return context


class DeleteTwittView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Twitt
    success_url = reverse_lazy("twitter-home")
    success_message = "Unfortunately, you succeeded in deleting your own Twitt!"

    def delete(self, request, *args, **kwargs):
        messages.success(request, self.success_message)
        return super().delete(self, request, *args, **kwargs)

    def test_func(self):
        return self.get_object().author == self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Delete twitt"
        return context

@login_required
def ajax_modify_twitt(request, pk):
    if request.method == 'POST' and request.is_ajax():
        new_content = request.POST.get('new_content')
        twitt = get_object_or_404(Twitt.objects.all(), pk=pk)

        twitt.content = new_content
        twitt.save()

        return JsonResponse({})
    else:
        raise Http404("This method is not allowed. Try again using POST.")
