# Generated by Django 2.2.2 on 2019-06-22 20:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='image',
            field=models.ImageField(default='images/default.png', upload_to='images/'),
        ),
    ]
