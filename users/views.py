from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, DetailView, UpdateView
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from .models import Profile
from .forms import UserUpdateForm, UserRegisterForm


# Create your views here.
class SignUpView(CreateView):
    form_class = UserRegisterForm
    template_name = "users/register.html"

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "User creation completed successfully. Now you can sign in.")
        return redirect("login")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Sign up"
        return context


@login_required
def update_user_view(request):
    if request.method == "POST":
        user_form = UserUpdateForm(request.POST, instance=request.user)

        if user_form.is_valid():
            user_form.save()
            messages.success(request, "Personal update complete")
            return redirect("user-update")
    else:
        user_form = UserUpdateForm(instance=request.user)

    return render(request, "users/user_form.html", {"user_form": user_form, "title": "Change yourself"})


@login_required
def change_password_view(request):
    if request.method == "POST":
        password_form = PasswordChangeForm(request.user, request.POST)

        if password_form.is_valid():
            user = password_form.save()
            update_session_auth_hash(request, user)
            messages.success(request, "Password changed successfully")
            return redirect("change-password")
    else:
        password_form = PasswordChangeForm(request.user)

    return render(request, "users/change_password.html", {"password_form": password_form, "title": "Change password"})


class ShowProfileView(LoginRequiredMixin, DetailView):
    model = Profile

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Profile details"
        return context


class UpdateProfileview(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Profile
    fields = ['description', 'image']

    def form_valid(self, form):
        form.instance.user = self.request.user
        messages.success(self.request, "You profile is now better than ever!")
        return super().form_valid(form)

    def test_func(self):
        return self.get_object().user == self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Update profile"
        return context
