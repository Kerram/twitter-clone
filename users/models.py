from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse_lazy


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField()
    image = models.ImageField(upload_to='images/',
                              default='images/default.png')

    def get_absolute_url(self):
        return reverse_lazy("profile-show", kwargs={"pk": self.pk})
