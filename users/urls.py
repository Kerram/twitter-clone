from django.urls import path
from . import views

urlpatterns = [
    path("profile/show/<int:pk>/", views.ShowProfileView.as_view(), name="profile-show"),
    path("profile/update/<int:pk>/", views.UpdateProfileview.as_view(), name="profile-update"),
    path("me/update/", views.update_user_view, name="user-update"),
    path("me/change_password/", views.change_password_view, name="change-password")
]
